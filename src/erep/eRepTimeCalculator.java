package erep;

import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

public class eRepTimeCalculator {

	private static LocalDate	eRepLaunch		= new LocalDate(2007, 11, 20);
	private static DateTimeZone	eRepTimeZone	= DateTimeZone
														.forID("America/Los_Angeles");

	public static int geteRepDay() {
		LocalDate today = new LocalDate(eRepTimeZone);
		return new eRepTimeCalculator().daysBetween(eRepLaunch, today);
	}

	public static int toErepDate(LocalDate date) {
		return new eRepTimeCalculator().daysBetween(eRepLaunch, date);
	}

	private Integer daysBetween(LocalDate first, LocalDate last) {
		return Days.daysBetween(first, last).getDays();
	}

	public static int geteRepHour() {
		LocalTime eRepTime = new LocalTime(eRepTimeZone);
		return eRepTime.getHourOfDay();
	}

	public static void main(String... args) {
		System.out.println(eRepTimeCalculator.toErepDate(new LocalDate(2013,
				02, 26)));
	}
}
