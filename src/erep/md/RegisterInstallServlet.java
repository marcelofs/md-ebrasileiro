package erep.md;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import erep.md.data.InstallDAO;

public class RegisterInstallServlet extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(RegisterInstallServlet.class
															.getName());


	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		String version = req.getParameter("version").trim();

		new InstallDAO().newInstall(version);

		logger.log(Level.WARNING, "New install registered, version: " + version);

	}

}
