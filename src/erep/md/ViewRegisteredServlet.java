package erep.md;

import java.io.IOException;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import erep.md.data.InstallDAO;

public class ViewRegisteredServlet extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(RegisterInstallServlet.class
															.getName());

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setCharacterEncoding("UTF-8");

		resp.getWriter()
				.write("<!doctype html>\n\n"
						+ "<html>\n<head>\n"
						+ "<meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n"
						+ "<title>MoD Script</title>\n</head>\n<body>");

		resp.getWriter().write(
				"\n<h1>Número de instalações por versão:</h1><br />\n");

		Map<String, Integer> results = new InstallDAO().getInstallCounts();

		TreeSet<String> orderedVersions = new TreeSet<String>(results.keySet());

		resp.getWriter().write("<ul>\n");
		for (String v : orderedVersions)
			resp.getWriter().write(
					"<li>" + v + ": " + results.get(v) + "</li>\n");
		resp.getWriter().write("</ul>");

		resp.getWriter().write("\n</body>\n</html>");
	}
}
