package erep.md;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class OrdersRedirectionServlet extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(OrdersRedirectionServlet.class
															.getName());

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.sendRedirect("https://docs.google.com/spreadsheet/pub?key=0AqkcCK_gTwjVdE5lRHlOa0c5M19nclRWQWJiOUlkWkE&single=true&gid=1&output=txt&range=b14");
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		doGet(req, resp);
	}

}
