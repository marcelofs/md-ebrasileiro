package erep.md;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.appengine.api.urlfetch.HTTPRequest;
import com.google.appengine.api.urlfetch.HTTPResponse;
import com.google.appengine.api.urlfetch.URLFetchService;
import com.google.appengine.api.urlfetch.URLFetchServiceFactory;

import erep.Mailer;
import erep.eRepTimeCalculator;

public class EBFarmersJob extends HttpServlet {
	private static final long	serialVersionUID	= 1L;

	private static final Logger	logger				= Logger.getLogger(EBFarmersJob.class
															.getName());

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		try {

			logger.log(Level.INFO, "Starting farmer countries update");

			List<String> countries = Arrays.asList("Brazil", "Chile",
					"Spain", "USA", "Turkey");

			Map<String, Future<HTTPResponse>> apiResponses = new HashMap<>();

			URLFetchService service = URLFetchServiceFactory
					.getURLFetchService();

			for (String country : countries) {
				String apiLink = "http://dev.erpkapi.appspot.com/economy?country={country}"
						.replace("{country}", country);
				HTTPRequest request = new HTTPRequest(new URL(apiLink));
				request.getFetchOptions().setDeadline(300d);
				apiResponses.put(country, service.fetchAsync(request));
			}

			int day = eRepTimeCalculator.geteRepDay();
			StringBuilder builder = new StringBuilder("<html><body>")
					.append("<center><img src=\"http://myerepublikbot.appspot.com/logo-beta.cache.png\" /></center><br/>")
					.append("<h2> Salários - Dia ").append(day)
					.append("</h2><br>");

			JSONParser parser = new JSONParser();
			for (String country : countries) {
				builder.append("<br/><b>").append(country)
						.append("</b><br />Work Tax: ");

				HTTPResponse response = apiResponses.get(country).get();
				JSONObject obj = (JSONObject) parser.parse(new String(response
						.getContent()));

				JSONObject taxes = (JSONObject) obj.get("taxes");
				builder.append(taxes.get("workTax").toString()).append(
						"%<br />Salário Médio: ");

				JSONObject salary = (JSONObject) obj.get("salary");
				builder.append(salary.get("average").toString())
						.append("cc<br/>");

			}

			Mailer mailer = new Mailer();

			mailer.mail("Salarios eRepublik - Dia " + day, builder.toString(),
					mailer.new MailAddress("MadGhost", "rtvgaddini@gmail.com"));
			mailer.mail("Salarios eRepublik - Dia " + day, builder.toString(),
					mailer.new MailAddress("BigBoss77", "jvictorcbs@hotmail.com"));

			logger.log(Level.INFO, "Email with economy info sent");

		} catch (Exception e) {
			throw new IOException(e);
		}
	}
}
