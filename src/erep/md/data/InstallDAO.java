package erep.md.data;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import erep.PMF;

public class InstallDAO {

	private final PersistenceManager	pm	= PMF.getNewPersistenceManager();

	public void newInstall(String version) {

		RegisteredInstallation install = new RegisteredInstallation();
		install.setDate(new Date());
		install.setVersionId(getVersion(version).getId());

		pm.makePersistent(install);

	}

	public Map<String, Integer> getInstallCounts() {
		Map<String, Integer> results = new HashMap<String, Integer>();

		for (RegisteredVersion v : getAllVersions())
			results.put(v.getVersionName(), getInstallCount(v));

		return results;
	}

	@SuppressWarnings("unchecked")
	private Collection<RegisteredVersion> getAllVersions() {
		Query q = pm.newQuery(RegisteredVersion.class);
		return pm.detachCopyAll((List<RegisteredVersion>) q.execute());
	}

	@SuppressWarnings("unchecked")
	private Integer getInstallCount(RegisteredVersion version) {
		Query q = pm.newQuery(RegisteredInstallation.class);
		q.setFilter("versionId == " + version.getId() + "");
		return ((List<RegisteredInstallation>) q.execute()).size();
	}

	private RegisteredVersion getVersion(String version) {
		Query q = pm.newQuery(RegisteredVersion.class);
		q.setFilter("versionName == '" + version + "'");
		q.setUnique(true);

		RegisteredVersion result = (RegisteredVersion) q.execute();
		if (result != null)
			return pm.detachCopy(result);
		else
			return createVersion(version);
	}

	private RegisteredVersion createVersion(String version) {
		RegisteredVersion v = new RegisteredVersion();
		v.setVersionName(version);
		pm.makePersistent(v);

		return pm.detachCopy(v);
	}

}
